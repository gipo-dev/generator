<?php

namespace App\Services;

use App\Models\Project;
use App\Models\UsedGroup;

class GroupService
{
    /**
     * @param \App\Models\Project $project
     * @return \Illuminate\Support\Collection|UsedGroup[]
     */
    public function getUsed(Project $project)
    {
        return UsedGroup::where('project_id', $project->id)->get();
    }

    /**
     * @param \App\Models\Project $project
     * @param $groupId
     * @return void
     */
    public function setUsed(Project $project, $groupId)
    {
        UsedGroup::create([
            'project_id' => $project->id,
            'group_id' => $groupId,
        ]);
    }
}
