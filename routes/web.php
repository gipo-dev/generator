<?php

use App\Http\Controllers\IndexController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {

    Route::get('projects', [ProjectController::class, 'list'])->name('list');
    Route::get('{project}/group', [ProjectController::class, 'group'])->name('group');
    Route::post('{project}/download', [ProjectController::class, 'download'])->name('download');
    Route::get('{group}/generate', [PostController::class, 'generate'])->name('generate');
    Route::get('{group}/parts', [PostController::class, 'parts'])->name('parts');
    Route::post('{project}/save', [PostController::class, 'save'])->name('save');

});
