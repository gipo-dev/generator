<?php

namespace App\Services\Handlers;

use App\Models\Group;
use App\Models\Post;

class TopHeaders extends AbstractKeysHandler
{
    /**
     * @param \App\Models\Group $group
     * @return Post[]
     */
    public function generate(Group $group)
    {
        // получили ключи
        $headerKeys = collect([]);
        $group->posts->each(function (Post &$post) use (&$headerKeys) {
            $headerKeys = $headerKeys->merge($this->getWords($post->h_h1));
            $post->headerKeys = $this->getWords($post->h_h1);
        });

        //отрезаем лишние заголовки
        $keys = collect([]);
        foreach ($headerKeys as $key) {
            if (isset($keys[$key]))
                $keys[$key] += 1;
            else
                $keys[$key] = 1;
        }
        $keys = $keys->sortDesc()->take(round(count($keys) * .3));


        //сортировка
        foreach ($group->posts as &$post) {
            /** @var Post $post */
            foreach ($post->headerKeys as $headerKey) {
                if (isset($keys[$headerKey]))
                    $post->headerMark += 1;
            }
        }

        return $group->posts->sortByDesc('headerMark');
    }
}
