<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int
     */
    public $titleMark = 0;

    /**
     * @var array
     */
    public $titleKeys = [];

    /**
     * @var int
     */
    public $headerMark = 0;

    /**
     * @var string[]
     */
    public $headerKeys;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parts()
    {
        return $this->hasMany(PostPart::class);
    }
}
