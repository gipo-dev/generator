<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Services\GeneratorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZipArchive;

class ProjectController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        return response()->json(Project::get());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @param \App\Services\GeneratorService $generator
     * @return \Illuminate\Http\JsonResponse
     */
    public function group(Request $request, Project $project, GeneratorService $generator)
    {
        return response()->json([
            'posts_exists' => Storage::exists("projects/{$project->id}"),
            'group' => $generator->getGroup($project),
        ]);
    }

    public function download(Request $request, Project $project)
    {
        $path = "projects/{$project->id}";
        if (!Storage::exists($path)) {
            return false;
        }
        $path = storage_path('app/' . $path);

        $zip_file = 'projects/' . Str::slug($project->name) . ".zip";
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($path) + 1);
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();

        return response()->json($zip_file);

    }
}
