<?php

return [

    'properties' => [
        'name' => \App\Services\PostProperties\NameProperty::class,
        'content' => \App\Services\PostProperties\ContentProperty::class,
        'length' => \App\Services\PostProperties\LengthProperty::class,
        'subtitle' => \App\Services\PostProperties\SubtitleProperty::class,
        'img' => \App\Services\PostProperties\ImgProperty::class,
        'table' => \App\Services\PostProperties\TableProperty::class,
        'video' => \App\Services\PostProperties\VideoProperty::class,
    ],

];
