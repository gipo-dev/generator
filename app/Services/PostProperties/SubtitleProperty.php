<?php

namespace App\Services\PostProperties;

class SubtitleProperty extends AbstractPostProperty
{
    /**
     * @param $part
     * @return false|int|string
     */
    public function get($part)
    {
        preg_match_all('/<h(2|3|4|5|6)/', $part->content, $matches);
        return count($matches[0] ?? []);
    }
}
