<?php

namespace App\Services\Handlers;

use App\Models\Group;
use App\Models\PostPart;

class PartsFilter extends AbstractKeysHandler
{
    /**
     * @var int
     */
    private $uniqueness;

    /**
     * @param int $uniqueness
     */
    public function __construct($uniqueness)
    {
        $this->uniqueness = $uniqueness;
    }

    /**
     * @param \App\Models\Group $group
     * @return array
     */
    public function generate(Group $group)
    {
        $postParts = collect([]);
        foreach ($group->posts as $post) {
            /** @var \App\Models\Post $post */
            $postParts = $postParts->merge($post->parts);
        }
        return $this->removeDuplicates($postParts, $this->uniqueness)
            ->map(function ($part) {
                $properties = ['id' => $part->id, 'checked' => true];
                foreach (config('generator.properties') as $key => $generator) {
                    $properties[$key] = (new $generator)->get($part);
                }
                return $properties;
            })->toArray();
    }

    /**
     * @param string $val1
     * @param string $val2
     * @return float|int
     */
    private function stringOffset(string $val1, string $val2)
    {
        $set1 = array_unique(explode(' ', $val1));
        $set2 = array_unique(explode(' ', $val2));
        if (count($set2) > count($set1)) {
            $d = collect($set2)->filter(function ($item) use (&$set1) {
                return !in_array($item, $set1);
            });
        } else {
            $d = collect($set1)->filter(function ($item) use (&$set2) {
                return !in_array($item, $set2);
            });
        }
        return mb_strlen(implode(' ', $d->toArray())) * 2 / (mb_strlen($val1) + mb_strlen($val2)) * 100;
    }

    /**
     * @param PostPart[]|\Illuminate\Support\Collection $parts
     * @return array
     */
    private function calculateGroups($parts, $uniqueness)
    {
        $groups = [];
        $used = [];
        for ($i = 0; $i < count($parts); $i++) {
            $group = $parts->filter(function (PostPart $part) use ($i, &$parts, &$uniqueness, &$used) {
                $offset = $this->stringOffset($part->title, $parts[$i]->title);
                return $offset <= $uniqueness && !in_array($part->title, $used);
            });
            if ($group->count() < 1)
                continue;
            $groups[] = $group;
            foreach ($group as $item)
                $used[] = $item->title;
        }
        return $groups;
    }

    /**
     * @param $parts
     * @param $uniqueness
     * @return \Illuminate\Support\Collection
     */
    private function removeDuplicates($parts, $uniqueness)
    {
        $uniqueParts = collect([]);
        $groups = $this->calculateGroups($parts, $uniqueness);

        foreach ($groups as $group) {
            /** @var \Illuminate\Support\Collection $group */
            $uniqueParts[] = $group->sortBy(function ($postPart) {
                return mb_strlen($postPart->title);
            }, SORT_REGULAR, true)->first();
        }

        return $uniqueParts->sortBy(function (PostPart $part) {
            return $part->order;
        });
    }
}
