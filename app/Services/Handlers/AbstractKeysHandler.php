<?php

namespace App\Services\Handlers;

use App\Models\Group;

abstract class AbstractKeysHandler
{
    /**
     * @param string $text
     * @return string[]
     */
    public function getWords(string $text)
    {
        return array_filter(preg_split('/\,|\.|\!|\?|\;|\:|\s|\|/', $text));
    }

    abstract public function generate(Group $group);
}
