<?php

namespace App\Services\Handlers;

use App\Models\Group;
use App\Models\Post;

class TopTitles extends AbstractKeysHandler
{
    /**
     * @param \App\Models\Group $group
     * @return Post[]
     */
    public function generate($group)
    {
        // получили ключи
        $titleKeys = collect([]);
        $group->posts->each(function (Post &$post) use (&$titleKeys) {
            $titleKeys = $titleKeys->merge($this->getWords($post->h_title));
            $post->titleKeys = $this->getWords($post->h_title);
        });

        //отрезаем лишние заголовки
        $keys = collect([]);
        foreach ($titleKeys as $key) {
            if (isset($keys[$key]))
                $keys[$key] += 1;
            else
                $keys[$key] = 1;
        }
        $keys = $keys->sortDesc()->take(round(count($keys) * .3));


        //сортировка
        foreach ($group->posts as &$post) {
            foreach ($post->titleKeys as $titleKey) {
                if (isset($keys[$titleKey]))
                    $post->titleMark += 1;
            }
        }

        $group->posts = $group->posts->map(function (Post &$post) {
            $titleParts = explode(' ', $post->title);
            if (count($titleParts) > 6) {
                $titleParts = array_splice($titleParts, -2);
                foreach (['|', '-'] as $key) {
                    if (in_array($key, $titleParts)) {
                        $post->titleMark -= 3;
                    }
                }
            }
            return $post;
        })->filter(function (Post $post) {
            return !str_contains(mb_strtolower($post->title), 'купить');
        })->sortByDesc('titleMark');

        return $group->posts;
    }
}
