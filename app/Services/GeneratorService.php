<?php

namespace App\Services;

use App\Models\Group;
use App\Models\Post;
use App\Models\Project;
use App\Services\Handlers\PartsFilter;
use App\Services\Handlers\TopHeaders;
use App\Services\Handlers\TopTitles;

class GeneratorService
{
    /**
     * @var \App\Services\GroupService
     */
    private $groupService;

    /**
     * @param \App\Services\GroupService $groupService
     */
    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    /**
     * @return array[]
     */
    public function generateTitles(Group $group)
    {
        return [
            'titles' => $this->getTopTitles($group),
            'headers' => $this->getTopHeaders($group),
        ];
    }

    /**
     * @return array[]
     */
    public function generateParts(Group $group, $uniqueness)
    {
        return [
            'postParts' => $this->getPostParts($group, $uniqueness),
        ];
    }

    /**
     * @param \App\Models\Project $project
     * @return false|\Illuminate\Support\Collection
     */
    public function getGroup(Project $project)
    {
        $groups = $project->groups;
        $used = $this->groupService->getUsed($project)->pluck('group_id')->toArray();
        $free = collect([]);
        foreach ($groups as $item) {
            if (!in_array($item->id, $used))
                $free[] = $item;
        }
        if (count($free) > 0)
            return $free->random();
        return false;
    }

    /**
     * @return array
     */
    private function getPostParts(Group $group, $uniqueness)
    {
        return (new PartsFilter($uniqueness))->generate($group);
    }

    /**
     * @return array
     */
    private function getTopTitles(Group $group)
    {
        return (new TopTitles())->generate($group)
            ->map(function (Post $post) {
                return [
                    'name' => $post->title,
                    'mark' => $post->titleMark,
                ];
            });
    }

    /**
     * @return array
     */
    private function getTopHeaders(Group $group)
    {
        return (new TopHeaders())->generate($group)
            ->map(function (Post $post) {
                return [
                    'name' => $post->h1,
                    'mark' => $post->headerMark,
                ];
            });
    }
}
