/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
window.vSelect = require('vue-select').default;
window.vueDraggable = require('vuedraggable').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically regiser them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('v-select', vSelect);
Vue.component('draggable', vueDraggable);

Vue.component('projects-component', require('./components/ProjectsComponent.vue').default);
Vue.component('header-component', require('./components/HeaderComponent.vue').default);
Vue.component('generator-main', require('./components/GeneratorMainComponent.vue').default);
Vue.component('generator-preview', require('./components/GeneratorPreview.vue').default);

Vue.component('generator-titles', require('./components/GeneratorTitles.vue').default);
Vue.component('generator-headers', require('./components/GeneratorHeaders.vue').default);
Vue.component('generator-parts', require('./components/GeneratorParts.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: function () {
        return {
            components: {
                main: null,
                title: null,
                header: null,
                parts: null,
                preview: null,
            },
            currentProject: null,
            currentGroup: null,
            currentTitle: null,
            currentHeader: null,
            currentParts: [],
            canDownload: false,
        };
    },
    methods: {
        loadProject: function (project) {
            let that = this;
            that.components.main.isLoading = true;
            axios.get('/ajax/' + project.id + '/group')
                .then(function (resp) {
                    if (resp.data === false) {
                        alert('Все посты в этом проекте использованы');
                        return;
                    }

                    that.currentGroup = resp.data.group;
                    that.canDownload = resp.data.posts_exists;

                    axios.get('/ajax/' + that.currentGroup.id + '/generate').then(function (resp) {
                        that.components.title.titles = resp.data.titles;
                        that.components.header.headers = resp.data.headers;

                        that.components.title.current = resp.data.titles[0];
                        that.components.header.current = resp.data.headers[0];
                        that.components.main.isLoading = false;

                        that.loadParts();
                    });
                });
        },

        loadParts: function () {
            let that = this;
            that.components.main.isLoading = true;
            axios.get('/ajax/' + that.currentGroup.id + '/parts?u=' + that.components.parts.uniqueness)
                .then(function (resp) {
                    that.components.parts.parts = Object.values(resp.data.postParts);
                    that.components.preview.renderPreview();
                    that.components.main.isLoading = false;
                });
        },
    },
});
