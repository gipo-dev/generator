<?php

namespace App\Services\PostProperties;

class NameProperty extends AbstractPostProperty
{
    /**
     * @param $post
     * @return false|int|string
     */
    public function get($part)
    {
        return $part->title;
    }
}
