<?php

namespace App\Services\PostProperties;

class TableProperty extends AbstractPostProperty
{
    /**
     * @param $part
     * @return false|int|string
     */
    public function get($part)
    {
        preg_match_all('/<table/', $part->content, $matches);
        return count($matches[0] ?? []);
    }
}
