<?php

namespace App\Services\PostProperties;

class VideoProperty extends AbstractPostProperty
{
    /**
     * @param $part
     * @return int
     */
    public function get($part)
    {
        preg_match_all('/(<video|<iframe)/', $part->content, $matches);
        return count($matches[0] ?? []);
    }
}
