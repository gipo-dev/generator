<?php

namespace App\Providers;

use App\Services\GeneratorService;
use App\Services\GroupService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(GroupService::class, function () {
            return new GroupService();
        });
        $this->app->singleton(GeneratorService::class, function () {
            return new GeneratorService($this->app->make(GroupService::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
