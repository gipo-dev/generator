<?php

namespace App\Services\PostProperties;

class ImgProperty extends AbstractPostProperty
{
    /**
     * @param $part
     * @return false|int|string
     */
    public function get($part)
    {
        preg_match_all('/<img/', $part->content, $matches);
        return count($matches[0] ?? []);
    }
}
