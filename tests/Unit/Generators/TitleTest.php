<?php

namespace Tests\Unit\Generators;

use App\Models\Group;
use App\Models\Project;
use App\Services\GeneratorService;
use App\Services\Handlers\PartsFilter;
use App\Services\Handlers\TopHeaders;
use App\Services\Handlers\TopTitles;
use Tests\TestCase;

class TitleTest extends TestCase
{
    public function test_title_generator()
    {
        $generator = new TopTitles();
        $result = $generator->generate(Group::with(['posts', 'posts.parts'])->find(12736));
        dd($result);
        $this->assertTrue(true);
    }

    public function test_header_generator()
    {
        $generator = new TopHeaders();
        $result = $generator->generate(Group::with(['posts', 'posts.parts'])->find(12736));
        dd($result);
        $this->assertTrue(true);
    }

    public function test_parts_generator()
    {
        $generator = new PartsFilter(60);
        $result = $generator->generate(Group::with(['posts', 'posts.parts'])->find(12736));
        dd($result);
        $this->assertTrue(true);
    }

    public function test_group_selector()
    {
        /** @var GeneratorService $generator */
        $generator = $this->app->make(GeneratorService::class);
        $group = $generator->getGroup(Project::find(1952));
        dd($group);
    }
}
