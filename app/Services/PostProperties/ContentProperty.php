<?php

namespace App\Services\PostProperties;

class ContentProperty extends AbstractPostProperty
{
    /**
     * @param $post
     * @return false|int|string
     */
    public function get($part)
    {
        return "<h2 id='pt-{$part->id}'>" . $part->title . "</h2>" . $part->content;
    }
}
