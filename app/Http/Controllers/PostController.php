<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\PostPart;
use App\Models\Project;
use App\Services\GeneratorService;
use App\Services\GroupService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @param \App\Services\GeneratorService $generator
     * @return \Illuminate\Http\JsonResponse
     */
    public function generate(Request $request, Group $group, GeneratorService $generator)
    {
        return response()->json($generator->generateTitles($group));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @param \App\Services\GeneratorService $generator
     * @return \Illuminate\Http\JsonResponse
     */
    public function parts(Request $request, Group $group, GeneratorService $generator)
    {
        return response()->json($generator->generateParts($group, $request->u));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @param \App\Services\GroupService $groupService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function save(Request $request, Project $project, GroupService $groupService)
    {
        $groupService->setUsed($project, $request->get('group_id'));
        return response(
            Storage::put("projects/{$project->id}/" . (Str::slug($request->title)) . '.txt', $request->title . PHP_EOL . $request->get('content'))
        );
    }
}
