@extends('partials.body')

@section('content')
    @include('partials.header')
    <header-component></header-component>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <generator-main></generator-main>
            </div>
            <div class="col-md-6">
                <generator-preview></generator-preview>
            </div>
        </div>
    </div>
@endsection
