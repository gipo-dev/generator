<?php

namespace App\Services\PostProperties;

abstract class AbstractPostProperty
{
    /**
     * @param \App\Models\PostPart $part
     * @return string
     */
    abstract public function get($part);
}
