<?php

namespace App\Services\PostProperties;

class LengthProperty extends AbstractPostProperty
{
    /**
     * @param $part
     * @return false|int|string
     */
    public function get($part)
    {
        $content = strip_tags($part->content);
        return mb_strlen($content);
    }
}
